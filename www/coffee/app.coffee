angular.module('edify', ['ionic', 'ng-token-auth', 'ngCordova', 'angular-loading-bar', 'pusher-angular', 'edify.controllers', 'edify.directives'])

.run ($ionicPlatform, $cordovaSplashscreen, $rootScope, $state, $auth, $stateParams, $pusher) ->
  $ionicPlatform.ready ->
    
    # Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    # for form inputs)
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar true  if window.cordova and window.cordova.plugins.Keyboard
    cordova.plugins.Keyboard.disableScroll true if window.cordova and window.cordova.plugins.Keyboard
    StatusBar.styleLightContent() if window.StatusBar
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $auth.validateToken()
    # Configure Pusher
    client = new Pusher("ec8d9ce74d0b683e3dac",
      auth: 
        headers: $auth.retrieveData('auth_headers')
      # authEndpoint: 'http://localhost:3000/chat/authenticate',
      authEndpoint: 'http://edifyapp.herokuapp.com/chat/authenticate',
      # wsHost: 'mysterious-falls-4281.herokuapp.com',
      enabledTransports: ["ws", "flash"],
      disabledTransports: ["flash"]
    )
    window.pusher = $pusher(client)
    pusher.connection.bind 'connected', () ->
      $rootScope.socketId = pusher.connection.baseConnection.socket_id
    $rootScope.$on 'auth:validation-success', (ev, user) ->
      $state.go 'app.feed'
    $rootScope.$on 'auth:validation-error', (ev) ->
      $state.go 'login'
    $cordovaSplashscreen.hide()

.config [
  'cfpLoadingBarProvider' 
  (cfpLoadingBarProvider) ->
    cfpLoadingBarProvider.includeBar = false
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner" ng-style="spinnerStyle"><div class="spinner-icon" ng-style="iconStyle"></div></div>'
]

.config ($stateProvider, $urlRouterProvider, $authProvider, $compileProvider, $ionicConfigProvider) ->
  $stateProvider
    .state('login',
      templateUrl: 'templates/login.html',
      controller: 'AuthCtrl',
      resolve:
        auth: ($auth) ->
          return !$auth.validateUser()
        errors: () ->
          errors = { 
            noServerError: ""
            authError: ""
            registrationError: ""
            emailError: "Please enter a valid email address"
            passwordError: "Your password must be at least 6 characters"
          }
    )
    .state('app',
      controller: 'AppCtrl'
      abstract: true,
      templateUrl: 'templates/app.html'
    )
    .state('app.feed',
      url: '/feed'
      views: 
        'mainContent': 
          templateUrl: 'templates/app/feed.html',
          controller: 'FeedCtrl'
      resolve:
        auth: ($auth) ->
          return $auth.validateUser()
    )
    .state('app.newStory',
      url: '/stories/new',
      views: 
        'mainContent': 
          templateUrl: 'templates/stories/new.html',
          controller: 'NewStoryCtrl'
      resolve:
        auth: ($auth) ->
          return $auth.validateUser()
    )
    .state('app.inbox',
      url: '/inbox',
      views: 
        'mainContent': 
          templateUrl: 'templates/app/inbox.html',
          controller: 'InboxCtrl'
      resolve: 
        auth: ($auth) ->
          return $auth.validateUser()
    )
    .state('app.settings',
      url: '/settings',
      views:
        'mainContent':
          templateUrl: 'templates/app/settings.html',
          controller: 'SettingsCtrl'
      resolve: 
        auth: ($auth) ->
          return $auth.validateUser()
    )
    .state('app.chat',
      url: '/chat?channel&recipient',
      views:
        'mainContent':
          templateUrl: 'templates/chat.html',
          controller: 'ChatCtrl'
      resolve: 
        auth: ($auth) ->
          return $auth.validateUser()
    )
  $authProvider.configure
    apiUrl: 'http://192.168.56.101'
    # apiUrl: 'http://localhost:3000'
  $compileProvider.imgSrcSanitizationWhitelist /^\s*(https?|ftp|mailto|file|tel):/