angular.module("edify.controllers", ["edify.services"])

.controller 'AppCtrl', ($scope, $auth, $state, $templateCache, $pusher) ->
  $scope.logout = ->
    $auth.signOut().then( ->
      $templateCache.removeAll()
      channels = pusher.allChannels()
      if channels instanceof Array
        angular.forEach channels, (value, key) ->
          pusher.unsubscribe(value)
      else
        pusher.unsubscribe(channels[Object.keys(channels)].name)
      $state.go "login", {}, {reload: true}
    )
.controller 'AuthCtrl', ($scope, $auth, $state, $document, errors, $pusher, $rootScope) ->
  $scope.authenticationForm = {}
  $scope.loading = false
  $scope.errors = errors
  $scope.register = ->
    $scope.authenticationForm.password_confirmation = $scope.authenticationForm.password
    $auth.submitRegistration($scope.authenticationForm).then (response) ->
      $auth.submitLogin(
        email: $scope.authenticationForm.email,
        password: $scope.authenticationForm.password
      )
  $scope.$on 'auth:login-success', (ev, user) ->
    pusher.client.config.auth.headers = $auth.retrieveData('auth_headers')
    $state.go 'app.feed'
  $scope.$on 'auth:login-error', (ev, reason) ->
    $scope.noServerError = ""
    $scope.authError = ""
    if reason?
      $scope.errors.authError = "Invalid email or password"
    else
      $scope.errors.noServerError = "Sorry, we are experiencing difficulties. Please try again later."
  $scope.$on 'auth:registration-email-error', (ev, reason) ->
    if reason?
      $scope.errors.registrationError = reason.errors.email[0]
  $scope.$on 'cfpLoadingBar:started', () ->
    $scope.loading = true
  $scope.$on 'cfpLoadingBar:loading', () ->
    $document.find("body").find("div").eq(0).addClass("spinner-login").find("div").eq(0).addClass("icon-login")
  $scope.$on 'cfpLoadingBar:completed', () ->
    $scope.loading = false
  $scope.$on '$stateChangeStart', (event, unfoundState, fromState, fromParams) ->
    $scope.errors.authenticationForm = {}
    $scope.errors.loading = false
    $scope.errors.noServerError = ""
    $scope.errors.authError = ""
    $scope.errors.registrationError = ""
    $scope.errors.emailError = ""
    $scope.errors.passwordError = ""

.controller 'FeedCtrl', ($scope, $state, $rootScope, Story, $ionicSideMenuDelegate, $pusher, $location, $auth) ->
  $scope.user = $rootScope.user
  if !$scope.user.email?
    $state.go "login"
  angular.forEach $scope.user.subscribed_channels, (value, key) ->
    channel = pusher.subscribe(value.name)
  pusher.bind 'messageSent', (data) ->
    alert("Yo, you got another message from someone else!") unless $location.search().channel? # Alert unless user is the chatroom

  $scope.stories = Story.query( () ->
    angular.forEach $scope.stories, (value, key) ->
      if !value.image? or value.image.indexOf("missing") != -1
        value.image = "img/icon.png"
      $scope.stories[key].background = {'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + value.image + ')'}
      unless $scope.user.id == value.user.id
        voter_ids = value.votes_for.map((vote) -> vote.voter_id)
        if voter_ids.indexOf($scope.user.id) != -1
          $scope.stories[key].liked = true
  )

  $scope.refreshStories = ->
    Story.refresh().$promise
      .then( (result) ->
        $scope.stories = result
        angular.forEach $scope.stories, (value, key) ->
          if !value.image? or value.image.indexOf("missing") != -1
            value.image = "../img/icon.png"
          $scope.stories[key].background = {'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + value.image + ')'}
          unless $scope.user.id == value.user.id
            voter_ids = value.votes_for.map((vote) -> vote.voter_id)
            if voter_ids.indexOf($scope.user.id) != -1
              $scope.stories[key].liked = true
      )
      .finally( () ->
        $scope.$broadcast('scroll.refreshComplete')
      )
  $scope.like = (story) ->
    if story.liked
      return
    else
      story.liked = true
      Story.like({id: story.id}, 
        (value, response) ->
        (error) ->
          story.liked = false
      )
  $scope.unlike = (story) ->
    if story.liked
      story.liked = false
      Story.unlike({id: story.id},
        (value, response) ->
        (error) ->
          story.liked = true
      )
  $scope.goToChat = (story) ->
    channelName = [$scope.user.id, story.user.id].sort()
    $state.go('app.chat', { recipient: story.user.id, channel: "presence-" + channelName.join("") }) if story.user.id != $scope.user.id

.controller 'NewStoryCtrl', ($scope, $state, Story, $ionicModal, $cordovaCamera, $ionicActionSheet, Photo, $stateParams, $ionicScrollDelegate) ->
  $scope.newStory = {}
  $scope.image = {}
  $scope.image.oldQuery = ""
  $scope.photoUrls = {}
  $scope.photoUrls.photos = []
  $scope.textPlaceholder = "What is your story?"
  $scope.modalOpen = false
  $ionicModal.fromTemplateUrl("templates/modals/pictureSelection.html",
    scope: $scope
    animation: "slide-in-up"
  ).then (modal) ->
    $scope.photoSelectionModal = modal

  $scope.openModal = ->
    $scope.photoSelectionModal.show()
    $scope.modalOpen = true
  $scope.closeModal = ->
    $scope.photoSelectionModal.hide()

  $scope.$on "$destroy", ->
    $scope.photoSelectionModal.remove()
  
  $scope.getPhoto = (options) ->
    $cordovaCamera.getPicture(options).then (dataURL) ->
      dataURL = "data:image/jpeg;base64," + dataURL
      $scope.newStory.image = dataURL
      $scope.imageSrc = dataURL
      $scope.newStory.imageParams = {original_filename: $scope.imageSrc[-10..-1]}
      newStoryStyle = {
        'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + dataURL + ')',
        'padding-top': '25%',
        'text-align': 'center',
        'font-weight': 400,
        'color': 'white',
        'font-family': 'SourceSansPro'
      }
      $scope.newStory.background = newStoryStyle
      $scope.textPlaceholder = ""
      $scope.hideSheet()
      $scope.cloud = false
  
  $scope.chooseImageType = ->
      # Show the action sheet
    $scope.hideSheet = $ionicActionSheet.show(
      buttons: [
        {
          text: "Photo Library"
        },
        {
          text: "Take photo"
        }
      ]
      cancelText: "Cancel"
      buttonClicked: (index) ->
        if index == 0
          $scope.getPhoto({ sourceType: Camera.PictureSourceType.PHOTOLIBRARY, destinationType: Camera.DestinationType.DATA_URL, encodingType: Camera.EncodingType.JPEG })
        else if index == 1
          $scope.getPhoto({ sourcetype: Camera.PictureSourceType.CAMERA, destinationType: Camera.DestinationType.DATA_URL, encodingType: Camera.EncodingType.JPEG })
    )
  
  $scope.submitStory = ->
    newStoryData = {
      story: {
        text: $scope.newStory.text,
        image: $scope.newStory.image,
      }
    }
    Story.save(newStoryData, (value, response) ->
      $state.go('app.feed', {}, {reload: true})
    )

  $scope.submitStoryWithImage = ->
    newStoryData = {
      story: {
        text: $scope.newStory.text,
        image: $scope.newStory.image,
      }
      image_params: $scope.newStory.imageParams if $scope.newStory.imageParams
    }
    Story.save(newStoryData, (value, response) ->
      $state.go('app.feed', {}, {reload: true})
    )
  
  $scope.selectCloudImage = (event, photoUrl) ->
    $scope.newStory.image = photoUrl
    bigImage = photoUrl.replace("_q", "")
    $scope.newStory.image = bigImage
    newStoryStyle = {
      'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + bigImage + ')',
      'padding-top': '25%',
      'text-align': 'center',
      'font-weight': 400,
      'color': 'white',
      'font-family': 'SourceSansPro'
    }
    $scope.newStory.background = newStoryStyle
    $scope.textPlaceholder = ""
    $scope.closeModal()
    $scope.cloud = true
  
  $scope.search = (page = 1, picModal = false, isScroll = false) ->
    query = $scope.image.query
    query ?= ""
    $scope.openModal() if picModal # open the modal if this is in a modal
    if isScroll and $scope.photoUrls.photos.length == 0 # If the refresh is for scrolling and there are no photos on the page
      $scope.$broadcast('scroll.infiniteScrollComplete')  # Tell the spinner to stop
      return
    Photo.search_flickr({query: query, page: page}).$promise.then( (result) -> # Get images from flickr
      if query == $scope.image.oldQuery or isScroll # If the query is the same, then the request is for the next page
        angular.forEach result.photos, (value, key) ->
          $scope.photoUrls.photos.push value
      else
        $ionicScrollDelegate.scrollTop()
        $scope.photoUrls.photos = result.photos
      $scope.photoUrls.maxPages = result.pages
      $scope.photoUrls.page = page
      $scope.$broadcast('scroll.infiniteScrollComplete') unless picModal # Tell the spinner to stop unless this is happening in a modal
    )

  $scope.$watch('image.query', (newQuery, oldQuery) ->
    oldQuery = "" unless oldQuery?
    $scope.image.oldQuery = oldQuery
  )

  $scope.morePages = ->
    if $scope.photoUrls.photos.length != 0 and $scope.page < $scope.maxPages
      true
    else
      false
.controller 'InboxCtrl', ($scope, Chat) ->
  return
.controller 'SettingsCtrl', ($scope) ->
  return
.controller 'ChatCtrl', ($scope, Chat, $state, $stateParams, $location, $rootScope, $pusher, $ionicScrollDelegate) ->
  $scope.newMessage = {
    sender_id: $scope.user.id, 
    recipient_id: $stateParams.recipient,
    channel: $stateParams.channel
  }
  channel = pusher.channel($stateParams.channel)
  if channel.members.count <= 1
    $scope.status = "Offline"
    $scope.online = false
  else
    if channel.members.get($stateParams.recipient)?
      $scope.status = "Online"
      $scope.online = true
  $scope.messages = Chat.query({channel_name: $stateParams.channel}, () ->
    channel.bind 'messageSent', (data) ->
      $scope.addNewMessage(data)
    $ionicScrollDelegate.scrollBottom(false)
  )
  window.addEventListener 'native.keyboardshow', (ev) ->
    $ionicScrollDelegate.scrollBy(0, ev.keyboardHeight)
  window.addEventListener 'native.keyboardhide', (ev) ->
    $ionicScrollDelegate.scrollBottom(false)
  channel.bind 'pusher:member_added', (member) ->
    if member.id == parseInt($stateParams.recipient)
      $scope.status = "Online"
      $scope.online = true
  channel.bind 'pusher:member_removed', (member) ->
    if member.id == $stateParams.recipient and channel.members.count <= 1
      $scope.status = "Offline"
      $scope.online = false
  $scope.addNewMessage = (message) ->
    $scope.messages.push angular.extend({}, message)
    $ionicScrollDelegate.scrollBottom(true)
  $scope.sendMessage = ->
    if !$scope.newMessage.body? or $scope.newMessage.body.trim() == ""
      return
    $scope.newMessage.socket_id = $rootScope.socketId
    message = {}
    angular.copy($scope.newMessage, message) # Create a copy of the newMessage so the message input can be cleared
    $scope.newMessage.body = ""
    Chat.send({message: message})
    $scope.addNewMessage(message)

