baseUrl = "http://192.168.56.101/"
# baseUrl = "http://localhost:3000/"
angular.module('edify.services', ['ngResource'])

.factory 'Story', ($resource) ->
  storiesUrl = baseUrl + 'stories/:id/'
  $resource(storiesUrl, {id: '@id'},
    refresh:
      method: "GET",
      isArray: true,
      url: storiesUrl,
      ignoreLoadingBar: true
    like:
      headers: {'Content-Type': 'application/json'}, 
      method: 'POST',
      isArray: false,
      url: storiesUrl + 'like',
      ignoreLoadingBar: true
    unlike: 
      headers: {'Content-Type': 'application/json'},
      method: 'POST',
      isArray: false,
      url: storiesUrl + 'unlike',
      ignoreLoadingBar: true
  )

.factory 'Photo', ($resource) ->
  photosUrl = baseUrl + 'photos/'
  $resource(photosUrl, {},
    search_flickr:
      headers: {'Content-Type': 'application/json'},
      isArray: false,
      method: 'POST',
      url: photosUrl + 'search_flickr',
      ignoreLoadingBar: true
  )

.factory 'Chat', ($resource) ->
  messagesUrl = baseUrl + 'chat/'
  $resource(messagesUrl, {channel_name: '@channel_name'},
    send: 
      headers: {'Content-Type': 'application/json'},
      method: "POST",
      isArray: false,
      url: messagesUrl + 'send_message'
      ignoreLoadingBar: true
    query:
      method: 'GET',
      isArray: true,
      url: messagesUrl,
      headers: {'Content-Type': 'application/json'},
      ignoreLoadingBar: true
  )
