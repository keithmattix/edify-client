angular.module('edify', ['ionic', 'ng-token-auth', 'ngCordova', 'angular-loading-bar', 'pusher-angular', 'edify.controllers', 'edify.directives']).run(function($ionicPlatform, $cordovaSplashscreen, $rootScope, $state, $auth, $stateParams, $pusher) {
  return $ionicPlatform.ready(function() {
    var client;
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $auth.validateToken();
    client = new Pusher("ec8d9ce74d0b683e3dac", {
      auth: {
        headers: $auth.retrieveData('auth_headers')
      },
      authEndpoint: 'http://edifyapp.herokuapp.com/chat/authenticate',
      enabledTransports: ["ws", "flash"],
      disabledTransports: ["flash"]
    });
    window.pusher = $pusher(client);
    pusher.connection.bind('connected', function() {
      return $rootScope.socketId = pusher.connection.baseConnection.socket_id;
    });
    $rootScope.$on('auth:validation-success', function(ev, user) {
      return $state.go('app.feed');
    });
    $rootScope.$on('auth:validation-error', function(ev) {
      return $state.go('login');
    });
    return $cordovaSplashscreen.hide();
  });
}).config([
  'cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = false;
    return cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner" ng-style="spinnerStyle"><div class="spinner-icon" ng-style="iconStyle"></div></div>';
  }
]).config(function($stateProvider, $urlRouterProvider, $authProvider, $compileProvider, $ionicConfigProvider) {
  $stateProvider.state('login', {
    templateUrl: 'templates/login.html',
    controller: 'AuthCtrl',
    resolve: {
      auth: function($auth) {
        return !$auth.validateUser();
      },
      errors: function() {
        var errors;
        return errors = {
          noServerError: "",
          authError: "",
          registrationError: "",
          emailError: "Please enter a valid email address",
          passwordError: "Your password must be at least 6 characters"
        };
      }
    }
  }).state('app', {
    controller: 'AppCtrl',
    abstract: true,
    templateUrl: 'templates/app.html'
  }).state('app.feed', {
    url: '/feed',
    views: {
      'mainContent': {
        templateUrl: 'templates/app/feed.html',
        controller: 'FeedCtrl'
      }
    },
    resolve: {
      auth: function($auth) {
        return $auth.validateUser();
      }
    }
  }).state('app.newStory', {
    url: '/stories/new',
    views: {
      'mainContent': {
        templateUrl: 'templates/stories/new.html',
        controller: 'NewStoryCtrl'
      }
    },
    resolve: {
      auth: function($auth) {
        return $auth.validateUser();
      }
    }
  }).state('app.inbox', {
    url: '/inbox',
    views: {
      'mainContent': {
        templateUrl: 'templates/app/inbox.html',
        controller: 'InboxCtrl'
      }
    },
    resolve: {
      auth: function($auth) {
        return $auth.validateUser();
      }
    }
  }).state('app.settings', {
    url: '/settings',
    views: {
      'mainContent': {
        templateUrl: 'templates/app/settings.html',
        controller: 'SettingsCtrl'
      }
    },
    resolve: {
      auth: function($auth) {
        return $auth.validateUser();
      }
    }
  }).state('app.chat', {
    url: '/chat?channel&recipient',
    views: {
      'mainContent': {
        templateUrl: 'templates/chat.html',
        controller: 'ChatCtrl'
      }
    },
    resolve: {
      auth: function($auth) {
        return $auth.validateUser();
      }
    }
  });
  $authProvider.configure({
    apiUrl: 'http://192.168.56.101'
  });
  return $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
});

angular.module("edify.controllers", ["edify.services"]).controller('AppCtrl', function($scope, $auth, $state, $templateCache, $pusher) {
  return $scope.logout = function() {
    return $auth.signOut().then(function() {
      var channels;
      $templateCache.removeAll();
      channels = pusher.allChannels();
      if (channels instanceof Array) {
        angular.forEach(channels, function(value, key) {
          return pusher.unsubscribe(value);
        });
      } else {
        pusher.unsubscribe(channels[Object.keys(channels)].name);
      }
      return $state.go("login", {}, {
        reload: true
      });
    });
  };
}).controller('AuthCtrl', function($scope, $auth, $state, $document, errors, $pusher, $rootScope) {
  $scope.authenticationForm = {};
  $scope.loading = false;
  $scope.errors = errors;
  $scope.register = function() {
    $scope.authenticationForm.password_confirmation = $scope.authenticationForm.password;
    return $auth.submitRegistration($scope.authenticationForm).then(function(response) {
      return $auth.submitLogin({
        email: $scope.authenticationForm.email,
        password: $scope.authenticationForm.password
      });
    });
  };
  $scope.$on('auth:login-success', function(ev, user) {
    pusher.client.config.auth.headers = $auth.retrieveData('auth_headers');
    return $state.go('app.feed');
  });
  $scope.$on('auth:login-error', function(ev, reason) {
    $scope.noServerError = "";
    $scope.authError = "";
    if (reason != null) {
      return $scope.errors.authError = "Invalid email or password";
    } else {
      return $scope.errors.noServerError = "Sorry, we are experiencing difficulties. Please try again later.";
    }
  });
  $scope.$on('auth:registration-email-error', function(ev, reason) {
    if (reason != null) {
      return $scope.errors.registrationError = reason.errors.email[0];
    }
  });
  $scope.$on('cfpLoadingBar:started', function() {
    return $scope.loading = true;
  });
  $scope.$on('cfpLoadingBar:loading', function() {
    return $document.find("body").find("div").eq(0).addClass("spinner-login").find("div").eq(0).addClass("icon-login");
  });
  $scope.$on('cfpLoadingBar:completed', function() {
    return $scope.loading = false;
  });
  return $scope.$on('$stateChangeStart', function(event, unfoundState, fromState, fromParams) {
    $scope.errors.authenticationForm = {};
    $scope.errors.loading = false;
    $scope.errors.noServerError = "";
    $scope.errors.authError = "";
    $scope.errors.registrationError = "";
    $scope.errors.emailError = "";
    return $scope.errors.passwordError = "";
  });
}).controller('FeedCtrl', function($scope, $state, $rootScope, Story, $ionicSideMenuDelegate, $pusher, $location, $auth) {
  $scope.user = $rootScope.user;
  if ($scope.user.email == null) {
    $state.go("login");
  }
  angular.forEach($scope.user.subscribed_channels, function(value, key) {
    var channel;
    return channel = pusher.subscribe(value.name);
  });
  pusher.bind('messageSent', function(data) {
    if ($location.search().channel == null) {
      return alert("Yo, you got another message from someone else!");
    }
  });
  $scope.stories = Story.query(function() {
    return angular.forEach($scope.stories, function(value, key) {
      var voter_ids;
      if ((value.image == null) || value.image.indexOf("missing") !== -1) {
        value.image = "img/icon.png";
      }
      $scope.stories[key].background = {
        'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + value.image + ')'
      };
      if ($scope.user.id !== value.user.id) {
        voter_ids = value.votes_for.map(function(vote) {
          return vote.voter_id;
        });
        if (voter_ids.indexOf($scope.user.id) !== -1) {
          return $scope.stories[key].liked = true;
        }
      }
    });
  });
  $scope.refreshStories = function() {
    return Story.refresh().$promise.then(function(result) {
      $scope.stories = result;
      return angular.forEach($scope.stories, function(value, key) {
        var voter_ids;
        if ((value.image == null) || value.image.indexOf("missing") !== -1) {
          value.image = "../img/icon.png";
        }
        $scope.stories[key].background = {
          'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + value.image + ')'
        };
        if ($scope.user.id !== value.user.id) {
          voter_ids = value.votes_for.map(function(vote) {
            return vote.voter_id;
          });
          if (voter_ids.indexOf($scope.user.id) !== -1) {
            return $scope.stories[key].liked = true;
          }
        }
      });
    })["finally"](function() {
      return $scope.$broadcast('scroll.refreshComplete');
    });
  };
  $scope.like = function(story) {
    if (story.liked) {

    } else {
      story.liked = true;
      return Story.like({
        id: story.id
      }, function(value, response) {}, function(error) {
        return story.liked = false;
      });
    }
  };
  $scope.unlike = function(story) {
    if (story.liked) {
      story.liked = false;
      return Story.unlike({
        id: story.id
      }, function(value, response) {}, function(error) {
        return story.liked = true;
      });
    }
  };
  return $scope.goToChat = function(story) {
    var channelName;
    channelName = [$scope.user.id, story.user.id].sort();
    if (story.user.id !== $scope.user.id) {
      return $state.go('app.chat', {
        recipient: story.user.id,
        channel: "presence-" + channelName.join("")
      });
    }
  };
}).controller('NewStoryCtrl', function($scope, $state, Story, $ionicModal, $cordovaCamera, $ionicActionSheet, Photo, $stateParams, $ionicScrollDelegate) {
  $scope.newStory = {};
  $scope.image = {};
  $scope.image.oldQuery = "";
  $scope.photoUrls = {};
  $scope.photoUrls.photos = [];
  $scope.textPlaceholder = "What is your story?";
  $scope.modalOpen = false;
  $ionicModal.fromTemplateUrl("templates/modals/pictureSelection.html", {
    scope: $scope,
    animation: "slide-in-up"
  }).then(function(modal) {
    return $scope.photoSelectionModal = modal;
  });
  $scope.openModal = function() {
    $scope.photoSelectionModal.show();
    return $scope.modalOpen = true;
  };
  $scope.closeModal = function() {
    return $scope.photoSelectionModal.hide();
  };
  $scope.$on("$destroy", function() {
    return $scope.photoSelectionModal.remove();
  });
  $scope.getPhoto = function(options) {
    return $cordovaCamera.getPicture(options).then(function(dataURL) {
      var newStoryStyle;
      dataURL = "data:image/jpeg;base64," + dataURL;
      $scope.newStory.image = dataURL;
      $scope.imageSrc = dataURL;
      $scope.newStory.imageParams = {
        original_filename: $scope.imageSrc.slice(-10)
      };
      newStoryStyle = {
        'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + dataURL + ')',
        'padding-top': '25%',
        'text-align': 'center',
        'font-weight': 400,
        'color': 'white',
        'font-family': 'SourceSansPro'
      };
      $scope.newStory.background = newStoryStyle;
      $scope.textPlaceholder = "";
      $scope.hideSheet();
      return $scope.cloud = false;
    });
  };
  $scope.chooseImageType = function() {
    return $scope.hideSheet = $ionicActionSheet.show({
      buttons: [
        {
          text: "Photo Library"
        }, {
          text: "Take photo"
        }
      ],
      cancelText: "Cancel",
      buttonClicked: function(index) {
        if (index === 0) {
          return $scope.getPhoto({
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: Camera.DestinationType.DATA_URL,
            encodingType: Camera.EncodingType.JPEG
          });
        } else if (index === 1) {
          return $scope.getPhoto({
            sourcetype: Camera.PictureSourceType.CAMERA,
            destinationType: Camera.DestinationType.DATA_URL,
            encodingType: Camera.EncodingType.JPEG
          });
        }
      }
    });
  };
  $scope.submitStory = function() {
    var newStoryData;
    newStoryData = {
      story: {
        text: $scope.newStory.text,
        image: $scope.newStory.image
      }
    };
    return Story.save(newStoryData, function(value, response) {
      return $state.go('app.feed', {}, {
        reload: true
      });
    });
  };
  $scope.submitStoryWithImage = function() {
    var newStoryData;
    newStoryData = {
      story: {
        text: $scope.newStory.text,
        image: $scope.newStory.image
      },
      image_params: $scope.newStory.imageParams ? $scope.newStory.imageParams : void 0
    };
    return Story.save(newStoryData, function(value, response) {
      return $state.go('app.feed', {}, {
        reload: true
      });
    });
  };
  $scope.selectCloudImage = function(event, photoUrl) {
    var bigImage, newStoryStyle;
    $scope.newStory.image = photoUrl;
    bigImage = photoUrl.replace("_q", "");
    $scope.newStory.image = bigImage;
    newStoryStyle = {
      'background-image': 'linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(76, 89, 93, 0.33)), url(' + bigImage + ')',
      'padding-top': '25%',
      'text-align': 'center',
      'font-weight': 400,
      'color': 'white',
      'font-family': 'SourceSansPro'
    };
    $scope.newStory.background = newStoryStyle;
    $scope.textPlaceholder = "";
    $scope.closeModal();
    return $scope.cloud = true;
  };
  $scope.search = function(page, picModal, isScroll) {
    var query;
    if (page == null) {
      page = 1;
    }
    if (picModal == null) {
      picModal = false;
    }
    if (isScroll == null) {
      isScroll = false;
    }
    query = $scope.image.query;
    if (query == null) {
      query = "";
    }
    if (picModal) {
      $scope.openModal();
    }
    if (isScroll && $scope.photoUrls.photos.length === 0) {
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }
    return Photo.search_flickr({
      query: query,
      page: page
    }).$promise.then(function(result) {
      if (query === $scope.image.oldQuery || isScroll) {
        angular.forEach(result.photos, function(value, key) {
          return $scope.photoUrls.photos.push(value);
        });
      } else {
        $ionicScrollDelegate.scrollTop();
        $scope.photoUrls.photos = result.photos;
      }
      $scope.photoUrls.maxPages = result.pages;
      $scope.photoUrls.page = page;
      if (!picModal) {
        return $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    });
  };
  $scope.$watch('image.query', function(newQuery, oldQuery) {
    if (oldQuery == null) {
      oldQuery = "";
    }
    return $scope.image.oldQuery = oldQuery;
  });
  return $scope.morePages = function() {
    if ($scope.photoUrls.photos.length !== 0 && $scope.page < $scope.maxPages) {
      return true;
    } else {
      return false;
    }
  };
}).controller('InboxCtrl', function($scope, Chat) {}).controller('SettingsCtrl', function($scope) {}).controller('ChatCtrl', function($scope, Chat, $state, $stateParams, $location, $rootScope, $pusher, $ionicScrollDelegate) {
  var channel;
  $scope.newMessage = {
    sender_id: $scope.user.id,
    recipient_id: $stateParams.recipient,
    channel: $stateParams.channel
  };
  channel = pusher.channel($stateParams.channel);
  if (channel.members.count <= 1) {
    $scope.status = "Offline";
    $scope.online = false;
  } else {
    if (channel.members.get($stateParams.recipient) != null) {
      $scope.status = "Online";
      $scope.online = true;
    }
  }
  $scope.messages = Chat.query({
    channel_name: $stateParams.channel
  }, function() {
    channel.bind('messageSent', function(data) {
      return $scope.addNewMessage(data);
    });
    return $ionicScrollDelegate.scrollBottom(false);
  });
  window.addEventListener('native.keyboardshow', function(ev) {
    return $ionicScrollDelegate.scrollBy(0, ev.keyboardHeight);
  });
  window.addEventListener('native.keyboardhide', function(ev) {
    return $ionicScrollDelegate.scrollBottom(false);
  });
  channel.bind('pusher:member_added', function(member) {
    if (member.id === parseInt($stateParams.recipient)) {
      $scope.status = "Online";
      return $scope.online = true;
    }
  });
  channel.bind('pusher:member_removed', function(member) {
    if (member.id === $stateParams.recipient && channel.members.count <= 1) {
      $scope.status = "Offline";
      return $scope.online = false;
    }
  });
  $scope.addNewMessage = function(message) {
    $scope.messages.push(angular.extend({}, message));
    return $ionicScrollDelegate.scrollBottom(true);
  };
  return $scope.sendMessage = function() {
    var message;
    if (($scope.newMessage.body == null) || $scope.newMessage.body.trim() === "") {
      return;
    }
    $scope.newMessage.socket_id = $rootScope.socketId;
    message = {};
    angular.copy($scope.newMessage, message);
    $scope.newMessage.body = "";
    Chat.send({
      message: message
    });
    return $scope.addNewMessage(message);
  };
});

angular.module('edify.directives', []);

var baseUrl;

baseUrl = "http://192.168.56.101/";

angular.module('edify.services', ['ngResource']).factory('Story', function($resource) {
  var storiesUrl;
  storiesUrl = baseUrl + 'stories/:id/';
  return $resource(storiesUrl, {
    id: '@id'
  }, {
    refresh: {
      method: "GET",
      isArray: true,
      url: storiesUrl,
      ignoreLoadingBar: true
    },
    like: {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      isArray: false,
      url: storiesUrl + 'like',
      ignoreLoadingBar: true
    },
    unlike: {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      isArray: false,
      url: storiesUrl + 'unlike',
      ignoreLoadingBar: true
    }
  });
}).factory('Photo', function($resource) {
  var photosUrl;
  photosUrl = baseUrl + 'photos/';
  return $resource(photosUrl, {}, {
    search_flickr: {
      headers: {
        'Content-Type': 'application/json'
      },
      isArray: false,
      method: 'POST',
      url: photosUrl + 'search_flickr',
      ignoreLoadingBar: true
    }
  });
}).factory('Chat', function($resource) {
  var messagesUrl;
  messagesUrl = baseUrl + 'chat/';
  return $resource(messagesUrl, {
    channel_name: '@channel_name'
  }, {
    send: {
      headers: {
        'Content-Type': 'application/json'
      },
      method: "POST",
      isArray: false,
      url: messagesUrl + 'send_message',
      ignoreLoadingBar: true
    },
    query: {
      method: 'GET',
      isArray: true,
      url: messagesUrl,
      headers: {
        'Content-Type': 'application/json'
      },
      ignoreLoadingBar: true
    }
  });
});

angular.module('ng-token-auth', ['ipCookie']).provider('$auth', function() {
  var configs, defaultConfigName;
  configs = {
    "default": {
      apiUrl: '/api',
      signOutUrl: '/auth/sign_out',
      emailSignInPath: '/auth/sign_in',
      emailRegistrationPath: '/auth',
      accountUpdatePath: '/auth',
      accountDeletePath: '/auth',
      confirmationSuccessUrl: function() {
        return window.location.href;
      },
      passwordResetPath: '/auth/password',
      passwordUpdatePath: '/auth/password',
      passwordResetSuccessUrl: function() {
        return window.location.href;
      },
      tokenValidationPath: '/auth/validate_token',
      proxyIf: function() {
        return false;
      },
      proxyUrl: '/proxy',
      validateOnPageLoad: true,
      forceHardRedirect: false,
      storage: 'cookies',
      tokenFormat: {
        "access-token": "{{ token }}",
        "token-type": "Bearer",
        client: "{{ clientId }}",
        expiry: "{{ expiry }}",
        uid: "{{ uid }}"
      },
      parseExpiry: function(headers) {
        return (parseInt(headers['expiry'], 10) * 1000) || null;
      },
      handleLoginResponse: function(resp) {
        return resp.data;
      },
      handleAccountUpdateResponse: function(resp) {
        return resp.data;
      },
      handleTokenValidationResponse: function(resp) {
        return resp.data;
      },
      authProviderPaths: {
        github: '/auth/github',
        facebook: '/auth/facebook',
        google: '/auth/google_oauth2'
      }
    }
  };
  defaultConfigName = "default";
  return {
    configure: function(params) {
      var conf, defaults, fullConfig, i, j, k, label, len, v;
      if (params instanceof Array && params.length) {
        for (i = j = 0, len = params.length; j < len; i = ++j) {
          conf = params[i];
          label = null;
          for (k in conf) {
            v = conf[k];
            label = k;
            if (i === 0) {
              defaultConfigName = label;
            }
          }
          defaults = angular.copy(configs["default"]);
          fullConfig = {};
          fullConfig[label] = angular.extend(defaults, conf[label]);
          angular.extend(configs, fullConfig);
        }
        if (defaultConfigName !== "default") {
          delete configs["default"];
        }
      } else if (params instanceof Object) {
        angular.extend(configs["default"], params);
      } else {
        throw "Invalid argument: ng-token-auth config should be an Array or Object.";
      }
      return configs;
    },
    $get: [
      '$http', '$q', '$location', 'ipCookie', '$window', '$timeout', '$rootScope', '$interpolate', (function(_this) {
        return function($http, $q, $location, ipCookie, $window, $timeout, $rootScope, $interpolate) {
          return {
            header: null,
            dfd: null,
            user: {},
            mustResetPassword: false,
            listener: null,
            initialize: function() {
              this.initializeListeners();
              return this.addScopeMethods();
            },
            initializeListeners: function() {
              this.listener = angular.bind(this, this.handlePostMessage);
              if ($window.addEventListener) {
                return $window.addEventListener("message", this.listener, false);
              }
            },
            cancel: function(reason) {
              if (this.t != null) {
                $timeout.cancel(this.t);
              }
              if (this.dfd != null) {
                this.rejectDfd(reason);
              }
              return $timeout(((function(_this) {
                return function() {
                  return _this.t = null;
                };
              })(this)), 0);
            },
            destroy: function() {
              this.cancel();
              if ($window.removeEventListener) {
                return $window.removeEventListener("message", this.listener, false);
              }
            },
            handlePostMessage: function(ev) {
              var error;
              if (ev.data.message === 'deliverCredentials') {
                delete ev.data.message;
                this.handleValidAuth(ev.data, true);
                $rootScope.$broadcast('auth:login-success', ev.data);
              }
              if (ev.data.message === 'authFailure') {
                error = {
                  reason: 'unauthorized',
                  errors: [ev.data.error]
                };
                this.cancel(error);
                return $rootScope.$broadcast('auth:login-error', error);
              }
            },
            addScopeMethods: function() {
              $rootScope.user = this.user;
              $rootScope.authenticate = angular.bind(this, this.authenticate);
              $rootScope.signOut = angular.bind(this, this.signOut);
              $rootScope.destroyAccount = angular.bind(this, this.destroyAccount);
              $rootScope.submitRegistration = angular.bind(this, this.submitRegistration);
              $rootScope.submitLogin = angular.bind(this, this.submitLogin);
              $rootScope.requestPasswordReset = angular.bind(this, this.requestPasswordReset);
              $rootScope.updatePassword = angular.bind(this, this.updatePassword);
              $rootScope.updateAccount = angular.bind(this, this.updateAccount);
              if (this.getConfig().validateOnPageLoad) {
                return this.validateUser({
                  config: this.getSavedConfig()
                });
              }
            },
            submitRegistration: function(params, opts) {
              var successUrl;
              if (opts == null) {
                opts = {};
              }
              successUrl = this.getResultOrValue(this.getConfig(opts.config).confirmationSuccessUrl);
              angular.extend(params, {
                confirm_success_url: successUrl,
                config_name: this.getCurrentConfigName(opts.config)
              });
              return $http.post(this.apiUrl(opts.config) + this.getConfig(opts.config).emailRegistrationPath, params).success(function(resp) {
                return $rootScope.$broadcast('auth:registration-email-success', params);
              }).error(function(resp) {
                return $rootScope.$broadcast('auth:registration-email-error', resp);
              });
            },
            submitLogin: function(params, opts) {
              if (opts == null) {
                opts = {};
              }
              this.initDfd();
              $http.post(this.apiUrl(opts.config) + this.getConfig(opts.config).emailSignInPath, params).success((function(_this) {
                return function(resp) {
                  var authData;
                  _this.setConfigName(opts.config);
                  authData = _this.getConfig(opts.config).handleLoginResponse(resp, _this);
                  _this.handleValidAuth(authData);
                  return $rootScope.$broadcast('auth:login-success', _this.user);
                };
              })(this)).error((function(_this) {
                return function(resp) {
                  _this.rejectDfd({
                    reason: 'unauthorized',
                    errors: ['Invalid credentials']
                  });
                  return $rootScope.$broadcast('auth:login-error', resp);
                };
              })(this));
              return this.dfd.promise;
            },
            userIsAuthenticated: function() {
              return this.retrieveData('auth_headers') && this.user.signedIn && !this.tokenHasExpired();
            },
            requestPasswordReset: function(params, opts) {
              var successUrl;
              if (opts == null) {
                opts = {};
              }
              successUrl = this.getResultOrValue(this.getConfig(opts.config).passwordResetSuccessUrl);
              params.redirect_url = successUrl;
              if (opts.config != null) {
                params.config_name = opts.config;
              }
              return $http.post(this.apiUrl(opts.config) + this.getConfig(opts.config).passwordResetPath, params).success(function(resp) {
                return $rootScope.$broadcast('auth:password-reset-request-success', params);
              }).error(function(resp) {
                return $rootScope.$broadcast('auth:password-reset-request-error', resp);
              });
            },
            updatePassword: function(params) {
              return $http.put(this.apiUrl() + this.getConfig().passwordUpdatePath, params).success((function(_this) {
                return function(resp) {
                  $rootScope.$broadcast('auth:password-change-success', resp);
                  return _this.mustResetPassword = false;
                };
              })(this)).error(function(resp) {
                return $rootScope.$broadcast('auth:password-change-error', resp);
              });
            },
            updateAccount: function(params) {
              return $http.put(this.apiUrl() + this.getConfig().accountUpdatePath, params).success((function(_this) {
                return function(resp) {
                  var curHeaders, key, newHeaders, ref, updateResponse, val;
                  updateResponse = _this.getConfig().handleAccountUpdateResponse(resp);
                  curHeaders = _this.retrieveData('auth_headers');
                  angular.extend(_this.user, updateResponse);
                  if (curHeaders) {
                    newHeaders = {};
                    ref = _this.getConfig().tokenFormat;
                    for (key in ref) {
                      val = ref[key];
                      if (curHeaders[key] && updateResponse[key]) {
                        newHeaders[key] = updateResponse[key];
                      }
                    }
                    _this.setAuthHeaders(newHeaders);
                  }
                  return $rootScope.$broadcast('auth:account-update-success', resp);
                };
              })(this)).error(function(resp) {
                return $rootScope.$broadcast('auth:account-update-error', resp);
              });
            },
            destroyAccount: function(params) {
              return $http["delete"](this.apiUrl() + this.getConfig().accountUpdatePath, params).success((function(_this) {
                return function(resp) {
                  _this.invalidateTokens();
                  return $rootScope.$broadcast('auth:account-destroy-success', resp);
                };
              })(this)).error(function(resp) {
                return $rootScope.$broadcast('auth:account-destroy-error', resp);
              });
            },
            authenticate: function(provider, opts) {
              if (opts == null) {
                opts = {};
              }
              if (this.dfd == null) {
                this.setConfigName(opts.config);
                this.initDfd();
                this.openAuthWindow(provider, opts);
              }
              return this.dfd.promise;
            },
            setConfigName: function(configName) {
              if (configName == null) {
                configName = defaultConfigName;
              }
              return this.persistData('currentConfigName', configName, configName);
            },
            openAuthWindow: function(provider, opts) {
              var authUrl;
              authUrl = this.buildAuthUrl(provider, opts);
              if (this.useExternalWindow()) {
                return this.requestCredentials(this.createPopup(authUrl));
              } else {
                return this.visitUrl(authUrl);
              }
            },
            visitUrl: function(url) {
              return $window.location.replace(url);
            },
            buildAuthUrl: function(provider, opts) {
              var authUrl, key, ref, val;
              if (opts == null) {
                opts = {};
              }
              authUrl = this.getConfig(opts.config).apiUrl;
              authUrl += this.getConfig(opts.config).authProviderPaths[provider];
              authUrl += '?auth_origin_url=' + encodeURIComponent($window.location.href);
              if (opts.params != null) {
                ref = opts.params;
                for (key in ref) {
                  val = ref[key];
                  authUrl += '&';
                  authUrl += encodeURIComponent(key);
                  authUrl += '=';
                  authUrl += encodeURIComponent(val);
                }
              }
              return authUrl;
            },
            requestCredentials: function(authWindow) {
              if (authWindow.closed) {
                this.cancel({
                  reason: 'unauthorized',
                  errors: ['User canceled login']
                });
                return $rootScope.$broadcast('auth:window-closed');
              } else {
                authWindow.postMessage("requestCredentials", "*");
                return this.t = $timeout(((function(_this) {
                  return function() {
                    return _this.requestCredentials(authWindow);
                  };
                })(this)), 500);
              }
            },
            createPopup: function(url) {
              return $window.open(url);
            },
            resolveDfd: function() {
              this.dfd.resolve(this.user);
              return $timeout(((function(_this) {
                return function() {
                  _this.dfd = null;
                  if (!$rootScope.$$phase) {
                    return $rootScope.$digest();
                  }
                };
              })(this)), 0);
            },
            validateUser: function(opts) {
              var clientId, configName, expiry, token, uid;
              if (opts == null) {
                opts = {};
              }
              configName = opts.config;
              if (this.dfd == null) {
                this.initDfd();
                if (this.userIsAuthenticated()) {
                  this.resolveDfd();
                } else {
                  if ($location.search().token !== void 0) {
                    token = $location.search().token;
                    clientId = $location.search().client_id;
                    uid = $location.search().uid;
                    expiry = $location.search().expiry;
                    configName = $location.search().config;
                    this.setConfigName(configName);
                    this.mustResetPassword = $location.search().reset_password;
                    this.firstTimeLogin = $location.search().account_confirmation_success;
                    this.setAuthHeaders(this.buildAuthHeaders({
                      token: token,
                      clientId: clientId,
                      uid: uid,
                      expiry: expiry
                    }));
                    $location.url($location.path() || '/');
                  } else if (this.retrieveData('currentConfigName')) {
                    configName = this.retrieveData('currentConfigName');
                  }
                  if (!isEmpty(this.retrieveData('auth_headers'))) {
                    if (this.tokenHasExpired()) {
                      $rootScope.$broadcast('auth:session-expired');
                      this.rejectDfd({
                        reason: 'unauthorized',
                        errors: ['Session expired.']
                      });
                    } else {
                      this.validateToken({
                        config: configName
                      });
                    }
                  } else {
                    this.rejectDfd({
                      reason: 'unauthorized',
                      errors: ['No credentials']
                    });
                    $rootScope.$broadcast('auth:invalid');
                  }
                }
              }
              return this.dfd.promise;
            },
            validateToken: function(opts) {
              if (opts == null) {
                opts = {};
              }
              if (!this.tokenHasExpired()) {
                return $http.get(this.apiUrl(opts.config) + this.getConfig(opts.config).tokenValidationPath).success((function(_this) {
                  return function(resp) {
                    var authData;
                    authData = _this.getConfig(opts.config).handleTokenValidationResponse(resp);
                    _this.handleValidAuth(authData);
                    if (_this.firstTimeLogin) {
                      $rootScope.$broadcast('auth:email-confirmation-success', _this.user);
                    }
                    if (_this.mustResetPassword) {
                      $rootScope.$broadcast('auth:password-reset-confirm-success', _this.user);
                    }
                    return $rootScope.$broadcast('auth:validation-success', _this.user);
                  };
                })(this)).error((function(_this) {
                  return function(data) {
                    if (_this.firstTimeLogin) {
                      $rootScope.$broadcast('auth:email-confirmation-error', data);
                    }
                    if (_this.mustResetPassword) {
                      $rootScope.$broadcast('auth:password-reset-confirm-error', data);
                    }
                    $rootScope.$broadcast('auth:validation-error', data);
                    return _this.rejectDfd({
                      reason: 'unauthorized',
                      errors: data.errors
                    });
                  };
                })(this));
              } else {
                return this.rejectDfd({
                  reason: 'unauthorized',
                  errors: ['Expired credentials']
                });
              }
            },
            tokenHasExpired: function() {
              var expiry, now;
              expiry = this.getExpiry();
              now = new Date().getTime();
              return expiry && expiry < now;
            },
            getExpiry: function() {
              return this.getConfig().parseExpiry(this.retrieveData('auth_headers') || {});
            },
            invalidateTokens: function() {
              var key, ref, val;
              ref = this.user;
              for (key in ref) {
                val = ref[key];
                delete this.user[key];
              }
              this.deleteData('currentConfigName');
              return this.deleteData('auth_headers');
            },
            signOut: function() {
              return $http["delete"](this.apiUrl() + this.getConfig().signOutUrl).success((function(_this) {
                return function(resp) {
                  _this.invalidateTokens();
                  return $rootScope.$broadcast('auth:logout-success');
                };
              })(this)).error((function(_this) {
                return function(resp) {
                  _this.invalidateTokens();
                  return $rootScope.$broadcast('auth:logout-error', resp);
                };
              })(this));
            },
            handleValidAuth: function(user, setHeader) {
              if (setHeader == null) {
                setHeader = false;
              }
              if (this.t != null) {
                $timeout.cancel(this.t);
              }
              angular.extend(this.user, user);
              this.user.signedIn = true;
              this.user.configName = this.getCurrentConfigName();
              if (setHeader) {
                this.setAuthHeaders(this.buildAuthHeaders({
                  token: this.user.auth_token,
                  clientId: this.user.client_id,
                  uid: this.user.uid,
                  expiry: this.user.expiry
                }));
              }
              return this.resolveDfd();
            },
            buildAuthHeaders: function(ctx) {
              var headers, key, ref, val;
              headers = {};
              ref = this.getConfig().tokenFormat;
              for (key in ref) {
                val = ref[key];
                headers[key] = $interpolate(val)(ctx);
              }
              return headers;
            },
            persistData: function(key, val, configName) {
              switch (this.getConfig(configName).storage) {
                case 'localStorage':
                  return $window.localStorage.setItem(key, JSON.stringify(val));
                default:
                  return ipCookie(key, val, {
                    path: '/'
                  });
              }
            },
            retrieveData: function(key) {
              switch (this.getConfig().storage) {
                case 'localStorage':
                  return JSON.parse($window.localStorage.getItem(key));
                default:
                  return ipCookie(key);
              }
            },
            deleteData: function(key) {
              switch (this.getConfig().storage) {
                case 'localStorage':
                  return $window.localStorage.removeItem(key);
                default:
                  return ipCookie.remove(key, {
                    path: '/'
                  });
              }
            },
            setAuthHeaders: function(h) {
              var newHeaders;
              newHeaders = angular.extend(this.retrieveData('auth_headers') || {}, h);
              return this.persistData('auth_headers', newHeaders);
            },
            useExternalWindow: function() {
              return !(this.getConfig().forceHardRedirect || $window.isIE());
            },
            initDfd: function() {
              return this.dfd = $q.defer();
            },
            rejectDfd: function(reason) {
              this.invalidateTokens();
              if (this.dfd != null) {
                this.dfd.reject(reason);
                return $timeout(((function(_this) {
                  return function() {
                    return _this.dfd = null;
                  };
                })(this)), 0);
              }
            },
            apiUrl: function(configName) {
              if (this.getConfig(configName).proxyIf()) {
                return this.getConfig(configName).proxyUrl;
              } else {
                return this.getConfig(configName).apiUrl;
              }
            },
            getConfig: function(name) {
              return configs[this.getCurrentConfigName(name)];
            },
            getResultOrValue: function(arg) {
              if (typeof arg === 'function') {
                return arg();
              } else {
                return arg;
              }
            },
            getCurrentConfigName: function(name) {
              return name || this.getSavedConfig();
            },
            getSavedConfig: function() {
              var c, key;
              c = void 0;
              key = 'currentConfigName';
              if ($window.localStorage) {
                if (c == null) {
                  c = JSON.parse($window.localStorage.getItem(key));
                }
              }
              if (c == null) {
                c = ipCookie(key);
              }
              return c || defaultConfigName;
            }
          };
        };
      })(this)
    ]
  };
}).config([
  '$httpProvider', function($httpProvider) {
    var httpMethods, tokenIsCurrent, updateHeadersFromResponse;
    tokenIsCurrent = function($auth, headers) {
      var newTokenExpiry, oldTokenExpiry;
      oldTokenExpiry = Number($auth.getExpiry());
      newTokenExpiry = Number($auth.getConfig().parseExpiry(headers || {}));
      return newTokenExpiry >= oldTokenExpiry;
    };
    updateHeadersFromResponse = function($auth, resp) {
      var key, newHeaders, ref, val;
      newHeaders = {};
      ref = $auth.getConfig().tokenFormat;
      for (key in ref) {
        val = ref[key];
        if (resp.headers(key)) {
          newHeaders[key] = resp.headers(key);
        }
      }
      if (tokenIsCurrent($auth, newHeaders)) {
        return $auth.setAuthHeaders(newHeaders);
      }
    };
    $httpProvider.interceptors.push([
      '$injector', function($injector) {
        return {
          request: function(req) {
            $injector.invoke([
              '$http', '$auth', function($http, $auth) {
                var key, ref, results, val;
                if (req.url.match($auth.apiUrl())) {
                  ref = $auth.retrieveData('auth_headers');
                  results = [];
                  for (key in ref) {
                    val = ref[key];
                    results.push(req.headers[key] = val);
                  }
                  return results;
                }
              }
            ]);
            return req;
          },
          response: function(resp) {
            $injector.invoke([
              '$http', '$auth', function($http, $auth) {
                if (resp.config.url.match($auth.apiUrl())) {
                  return updateHeadersFromResponse($auth, resp);
                }
              }
            ]);
            return resp;
          },
          responseError: function(resp) {
            $injector.invoke([
              '$http', '$auth', function($http, $auth) {
                if (resp.config.url.match($auth.apiUrl())) {
                  return updateHeadersFromResponse($auth, resp);
                }
              }
            ]);
            return $injector.get('$q').reject(resp);
          }
        };
      }
    ]);
    httpMethods = ['get', 'post', 'put', 'patch', 'delete'];
    return angular.forEach(httpMethods, function(method) {
      var base;
      if ((base = $httpProvider.defaults.headers)[method] == null) {
        base[method] = {};
      }
      return $httpProvider.defaults.headers[method]['If-Modified-Since'] = '0';
    });
  }
]).run([
  '$auth', '$window', '$rootScope', function($auth, $window, $rootScope) {
    return $auth.initialize();
  }
]);

window.isOldIE = function() {
  var nav, out, version;
  out = false;
  nav = navigator.userAgent.toLowerCase();
  if (nav && nav.indexOf('msie') !== -1) {
    version = parseInt(nav.split('msie')[1]);
    if (version < 10) {
      out = true;
    }
  }
  return out;
};

window.isIE = function() {
  var nav;
  nav = navigator.userAgent.toLowerCase();
  return (nav && nav.indexOf('msie') !== -1) || !!navigator.userAgent.match(/Trident.*rv\:11\./);
};

window.isEmpty = function(obj) {
  var key, val;
  if (!obj) {
    return true;
  }
  if (obj.length > 0) {
    return false;
  }
  if (obj.length === 0) {
    return true;
  }
  for (key in obj) {
    val = obj[key];
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
};
